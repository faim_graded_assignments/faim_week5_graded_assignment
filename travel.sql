select count(Gender) from passenger where Distance >= 600;

select min(Price) from price where Bus_Type = 'Sleeper';

select  Passenger_name from passenger where Passenger_name like 'S%';

select p.Passenger_name,p.Boarding_City,p.Destination_City,p.Bus_Type,pr.price from passenger as p,price pr
where p.Distance = pr.Distance and p.Bus_Type = pr.Bus_type;

select p.Passenger_Name,pr.Price from passenger p, price pr where p.Bus_Type = 'Sitting' and p.Distance = 1000;

select pr.price from price as pr inner join passenger as p
on pr.Distance = p.Distance where p.Boarding_City = 'panaji' and p.Destination_City = 'Bengaluru' and p.Passenger_name = 'Pallavi';


select p.Price from price p inner join passenger ps on p.Distance = ps.Distance where ps.Boarding_City = 'panaji' and ps.Destination_City = 'Bengaluru';

select distinct Distance from passenger order by Distance asc;

select Passenger_name,(Distance / (select sum(Distance) from passenger) * 100) as percentage from passenger;

create view sqlView as select Passenger_Name from passenger where Category = 'AC';
select * from sqlView;

create procedure showPassengers()
select count(Passenger_name) from passenger where Bus_Type = 'Sleeper';
call showPassengers();

select * from passenger limit 5;
select * from price limit 5;